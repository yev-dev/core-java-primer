package io.github.yy.core.algorithms.java8;

import java.util.function.Function;

public class FunctionsTest {

	public static void main(String... args) {
		double value = 4.0;
		
		Calculator calc1 = (param) -> Math.sqrt(value);
		Calculator calc2 = param -> Math.sqrt(value);
		GenericCalculator<Double, Double> calc3 = param -> Math.abs(value);
		Function<Double,Double> calc4 = (t) -> Math.abs(value);
		Function<Double,Double> calc5 = new Function<Double,Double>() {

			@Override
			public Double apply(Double t) {
				// TODO Auto-generated method stub
				return Math.abs(t);
			}
		};
		
		calc5.apply(value);
		
	}
	public interface Calculator {
		public Double calculate(Double param);
	}
	public interface GenericCalculator<I,O> {
		public O calculate(I param);
	}
}
