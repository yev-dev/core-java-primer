package io.github.yy.core.concurrency.threads;

public class WaitingExample {

	public static void main(String[] args) {
		
		WaitingExample example = new WaitingExample();
		
		Thread thread = new Thread(example.new NotifyingThread());
		
		thread.start();
		
		synchronized (thread) {
			
			try {
				System.out.println("Waiting...");
				thread.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Done!");
	}
	
	class NotifyingThread extends Thread {
		
		public void run() {
			synchronized (this) {
				System.out.println("Notifying... ");
				notify();
			}
		}
	}

}
