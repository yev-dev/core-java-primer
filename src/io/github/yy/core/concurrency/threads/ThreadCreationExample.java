package io.github.yy.core.concurrency.threads;

public class ThreadCreationExample {

	public static void main(String[] args) {
		
		Thread threadA = new Thread(new Runnable() {

			@Override
			public void run() {
				String threadName = Thread.currentThread().getName();
				System.out.println("Awaiting for a notify");
			    try {
//			    	wait();
			    	System.out.println("Notify recieved");
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    System.out.println("Hello " + threadName);
				
			}
			
		} );
		
		Runnable task = () -> {
		    String threadName = Thread.currentThread().getName();
		    
		    System.out.println("Hello " + threadName);
		};
		
		Thread threadB = new Thread(task);
		
		Thread threadC = new Thread(() -> {
			String threadName = Thread.currentThread().getName();
			System.out.println("Hello " + threadName);
		}) ;
	
		threadA.start();
		threadB.start();
		threadC.start();
		
		try {
			threadA.join();
			threadB.join();
			threadC.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Finished processing " + Thread.currentThread().getName());
		
	}
}
