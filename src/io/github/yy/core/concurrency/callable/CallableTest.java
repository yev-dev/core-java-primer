package io.github.yy.core.concurrency.callable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableTest {

	public static void main(String... args) {

		final double value = 4.0;
		
//		Callable<Double> task = () -> {return value * 2;};
		
		Callable<Double> task = new CallableTest().new CallableTask(value);
		
		int nThreads = Runtime.getRuntime().availableProcessors() + 1;
		
		System.out.println("Number of threads is " + nThreads);
		
		ExecutorService threadPool = Executors.newFixedThreadPool(nThreads); 
		
		Future<Double> futureObj =  threadPool.submit(task);

		Double val = null ;
		
		try {
			val = futureObj.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		} 
		
		System.out.println(val);
		
		threadPool.shutdown();
	}

	public class CallableTask implements Callable<Double> {

		private final double value;

		public CallableTask(double value) {
			super();
			this.value = value;
		}

		@Override
		public Double call() throws Exception {
			System.out.println("Doing operation in Thread" + Thread.currentThread().getName());
			Thread.sleep(500);
			
			return Math.sqrt(value);
		}

	}
}

