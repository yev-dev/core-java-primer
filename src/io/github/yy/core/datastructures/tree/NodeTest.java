package io.github.yy.core.datastructures.tree;

import org.junit.Test;

import static org.junit.Assert.*;

public class NodeTest {

    @Test
    public void printInOrder() {

        Node node = new Node(2);

        node.insert(1);
        node.insert(3);

        node.printInOrder();
    }
}