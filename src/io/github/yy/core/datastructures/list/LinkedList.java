package io.github.yy.core.datastructures.list;

public class LinkedList {

    Node head;

    //Keep on moving to the next node checking if it exists
    public void append(int data) {

        if (head == null) {
            head = new Node(data);
        }

        Node currentNode = head;

        while (currentNode.next != null) {
            currentNode = currentNode.next;
        }
        currentNode.next = new Node(data);

    }


    public void prepand(int data) {

        Node newHead = new Node(data);

        newHead.next = this.head;

        head = newHead;

    }

    public void delete(int data) {

        if (head == null) {
            return;
        }

        //If the data is the head itself
        if (head.data == data) {
            head = head.next;
        }

        Node currentNode = head;

        while (currentNode.next != null) {

            if (currentNode.next.data == data) {
                currentNode.next = currentNode.next.next;
                return;
            }

            currentNode = currentNode.next;
        }
    }


}
