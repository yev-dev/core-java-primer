package io.github.yy.core.datastructures.list;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.junit.Test;

public class ListOperationsTest {

	@Test
	public void reverseLinkedList() {
		final LinkedList1<String> three = new LinkedList1<>("3", null);
		final LinkedList1<String> two = new LinkedList1<>("2", three);
		final LinkedList1<String> one = new LinkedList1<>("1", two);

		final LinkedList1<String> reversed = LinkedList1.reverse(one);

		assertEquals("3", reversed.getElement());
		assertEquals("2", reversed.getNext().getElement());
		assertEquals("1", reversed.getNext().getNext().getElement());
	}
	
	@Test
	public void shouldRemoveDuplicatesFromList(){
		 List<String> al = new ArrayList<String>();
	     al.add("AA");
	     al.add("BB");
	     al.add("AA");
	     al.add("BB");
	     al.add("CC");
	     al.add("CC");
	     
	     LinkedHashSet<String> lhs = new LinkedHashSet<String>(al);
	     
	     al.clear();
	     
	     assertEquals(lhs.size(),3);
	}

}
